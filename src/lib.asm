global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line
global print_error
section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov  rax, 60
    xor  rdi, rdi
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        jz .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rsi
    mov  rdx, rax
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push di
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop di
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1
    mov rdi, 1
    mov rsi, 10
    mov rdx, 1
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
 xor rax, rax
    xor r8, r8
    mov rax, rdi
    mov r9, 0xA
.loop:
    xor rdx, rdx
    div r9
    inc r8
    push rdx
    cmp rax, 0
    je .print_d
    jmp .loop
.print_d:
    pop rdx
    add rdx, 0x30
    mov rdi, rdx
    push rcx
    call print_char
    pop rcx
    dec r8
    jnz .print_d
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .pos
    .neg:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
    .pos:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:

    xor rcx, rcx
    .loop:
        mov al, [rdi + rcx]
        sub al, [rsi + rcx]
        cmp al, 0
        jne .return_false

        cmp byte [rdi + rcx], 0
        je .return_true

        inc rcx
        jmp .loop

    .return_false:
        xor rax, rax
        ret
    .return_true:
        xor rax, rax
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax

    push 0
    mov rsi, rsp
    xor rdi, rdi
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:

    push rbx
    push r12
    push r13
    xor rbx, rbx
    mov r12, rdi
    mov r13, rsi
    jmp .loop
    .ws:
        or rbx, rbx
        jnz .retword
    .loop:
        call read_char
        or al, al
        jz .retword
        cmp al, ' '
        je .ws
        cmp al, `\n`
        je .ws
        cmp al, `\t`
        je .ws
        inc rbx
        cmp rbx, r13
        ja .fail
        mov [r12+rbx-1], al
        jmp .loop
    .retword:
        mov byte [r12+rbx], 0
        mov rax, r12
        mov rdx, rbx
        jmp .ret
    .fail:
        xor rax, rax
    .ret:
        pop r13
        pop r12
        pop rbx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:

    xor rax, rax
    xor rsi, rsi
    xor rdx, rdx
    mov r11, 10
    .ST:
        push rsi
        mov sil, [rdi]
        test sil, sil
        je .END
        cmp sil, 10
        je .END
        cmp sil, '0'
        jb .END
        cmp sil, '9'
        ja .END
        sub sil, 0x30
        mul r11
        add rax, rsi
        inc rdi
        pop rsi
        inc rsi
        jmp .ST
    .END:
        pop rdx
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
cmp byte [rdi], '-'
 je .ngt
 call parse_uint
 ret
    .ngt:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax

    xor rcx, rcx

    .loop:
        cmp rcx, rdx
        jz .control
        mov r9b, byte[rdi+rcx]
        mov byte[rsi+rcx], r9b
        cmp r9b, 0
        inc rcx
        jz .break
        jmp .loop

    .control:
        mov rax, 0
        ret

    .break:
        mov rax, rcx
        ret

read_line:
    xor r8, r8
.loop:
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    cmp al, 0
    jz .success
    cmp al, 0xA
    jz .success
    inc r8
    cmp r8, rsi
    jz .overflow
    mov [rdi + r8 - 1], rax
    jmp .loop 
.overflow:
    xor rax, rax
    jmp .end
.success:
    mov rax, rdi
    mov rdx, r8
.end:
    ret

print_error:
    push rdi
    call string_length
    pop rsi
    mov rdi, 2
    mov rdx, rax
    mov rax, 1
    syscall
    call print_newline
    jmp exit
