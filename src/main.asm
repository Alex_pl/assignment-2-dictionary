%include "src/colon.inc"
%include "src/lib.inc"
%include "src/dict.inc"
%include "src/words.inc"
%define buffer_size 255

global _start

section .bss
buffer: resb buffer_size

section .rodata
err_msg: db "Can't find el", 10, 0 ;

section .text
_start:
    sub rsp, buffer_size
    mov rdi, rsp
    mov rsi, buffer_size
    call read_line

    mov rsi, PTR
    mov rdi, rax
    call find_word

    add rsp, buffer_size

    test rax, rax
    jz .fail

    mov rdi, rax
    call string_length

    add rdi, rax
    inc rdi
    call print_string
    call print_newline

    xor rdi, rdi
    call exit

.fail:
    mov rdi, err_msg
    call print_error
    call print_newline
    mov rdi, 1
    call exit
