%define PTR 0

%macro colon 2
%%next: dq PTR
%ifstr %1
db %1,0
%else
%error "uncorrect argument"
%endif
%ifid %2
%2:
%else
%error "uncorrect second argument"
%endif
%define PTR %%next
%endmacro