%include "lib.inc"
%include "colon.inc"
%define link_size 8

global find_word


section .text

find_word:
	.loop:
		cmp rsi, 0
		je .not_found
		add rsi, link_size
		push rdi
		push rsi
		call string_equals
		pop rsi
		pop rdi
		sub rsi, link_size
		cmp rax, 1
		je .found
		mov rsi, [rsi]
		jmp .loop
	.found:
		mov rax, rsi
		ret
	.not_found:
		xor rax, rax
		ret
