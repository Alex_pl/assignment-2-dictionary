ASM=nasm
ASMFLAGS=-f elf64
LD=ld
LDFLAGS=-o

target: clean out/result

target/%.o: src/%.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

out/result: target/main.o target/dict.o target/lib.o
	$(LD) $(LDFLAGS) target/result target/lib.o target/dict.o target/main.o

clean:
	rm target/*
	rm out/result

.PHONY: target clean
